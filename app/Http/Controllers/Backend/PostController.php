<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class PostController extends Controller
{
    //

    public function index() {
        $posts = Http::get("https://jsonplaceholder.typicode.com/posts");

        return view("posts.index", [
            "posts" => json_decode($posts)
        ]);
    }
}
